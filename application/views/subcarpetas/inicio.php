
		<center>
			<h1> <strong>LIBRES AL VOTO</strong></h1>
		</center>


		<div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
  <div class="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
  </div>
	<center>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src=" <?php echo base_url(); ?>/assest/imagenes/eleccion.png " class="d-block w-50" alt="...">
    </div>
    <div class="carousel-item">
      <img src="<?php echo base_url(); ?>/assest/imagenes/eleccion1.png " class="d-block w-50" alt="...">
    </div>
    <div class="carousel-item">
      <img src=" <?php echo base_url(); ?>/assest/imagenes/eleccion2.png " class="d-block w-50" alt="...">
    </div>
  </div>
	</center>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div>

<br>

	<div class="row">
		<div class="col-md-4">
			<center>
				<br>
				<br>
				<br>
						<h3> <strong> QUE SON LAS ELECCIONES? </strong> </h3>
					<p>Las elecciones son un proceso institucional en el que los electores eligen con su voto, entre una pluralidad de candidatos a quienes ocuparán los cargos políticos en una democracia representativa.</p>
			</center>
		</div>
<br>
		<div class="col-md-4">
			<center>
				<br>
						<h3> <strong> CUANDO SON LAS ELECCIONES? </strong> </h3>
					<p>Las elecciones presidenciales de Ecuador de 2023 serán un evento electoral convocado por el presidente Guillermo Lasso tras la disolución de la Asamblea Nacional según el artículo 148 de la Constitución Nacional, denominado «muerte cruzada», siendo convocadas elecciones para renovar la Presidencia, Vicepresidencia y Asamblea Nacional para concluir el período constitucional 2021-2025.
						Por su carácter extraordinario, estas elecciones tendrán un proceso abreviado. El nuevo presidente y vicepresidente serán posesionados en sus cargos el 25 de noviembre de 2023 tras recibir las credenciales electorales.
					</p>
			</center>
		</div>
<br>
		<div class="col-md-4">
			<center>
				<h3> <strong>COMO VOTAR EN LAS ELECCIONES 2023?</strong> </h3>
			<p>
				En las próximas elecciones del 5 de febrero de 2023 se elegirán a alcaldes, prefectos, concejales, vocales de las juntas parroquiales y a siete vocales del Consejo de Participación Ciudadana y Control Social (Cpccs). Por esta razón, se tiene previsto que haya hasta nueve papeletas en las elecciones 2023 dependiendo de la zona.
				El primer requisito para poder sufragar es tener la cédula de ciudadanía vigente. Este documento se pedirá en la mesa electoral en la cual haya sido designada una persona. Mediante la página web o aplicación del CNE se puede conocer de manera rápida el recinto electoral al que debe acudir a votar.
			</p>
			</center>
		</div>
	</div>


<center>
<div class="row">
	<div class="col-md-4">
		<div class="card" style="width: 18rem;">
		<img src=" <?php echo base_url(); ?>/assest/imagenes/voto1.png" class="card-img-top" alt="...">
		<div class="card-body">
			<h5 class="card-title"> <strong>Regístrate para votar</strong></h5>
			<p class="card-text">Asegúrate de estar registrado en el padrón electoral. Si no lo estás, infórmate sobre los requisitos.</p>
			<a href="<?php echo base_url() ?>/assest/imagenes/voto1.png" class="btn btn-primary">ABRIR IMAGEN</a>
		</div>
	</div>
	</div>

	<div class="col-md-4">
		<div class="card" style="width: 18rem;">
		<img src=" <?php echo base_url(); ?>/assest/imagenes/voto2.png" class="card-img-top" alt="...">
		<div class="card-body">
			<h5 class="card-title"> <strong>Conoce tus opciones</strong> </h5>
			<p class="card-text">Investiga y familiarízate con los candidatos y partidos políticos que se postulan para las elecciones.</p>
			<a href=" <?php echo base_url(); ?>/assest/imagenes/voto2.png" class="btn btn-primary">ABRIR IMAGEN</a>
		</div>
	</div>
	</div>

	<div class="col-md-4">
		<div class="card" style="width: 18rem;">
		<img src=" <?php echo base_url(); ?>/assest/imagenes/voto3.png" class="card-img-top" alt="...">
		<div class="card-body">
			<h5 class="card-title"> <strong>Compara las promesas y acciones pasadas</strong> </h5>
			<p class="card-text">Analiza las promesas de los candidatos, e investiga su historia</p>
			<a href=" <?php echo base_url(); ?>/assest/imagenes/voto3.png " class="btn btn-primary">ABRIR IMAGEN</a>
		</div>
	</div>
	</div>
</div>

<br>

<div class="row">
	<div class="col-md-4">
		<div class="card" style="width: 18rem;">
		<img src=" <?php echo base_url(); ?>/assest/imagenes/voto4.png" class="card-img-top" alt="...">
		<div class="card-body">
			<h5 class="card-title"> <strong>Contexto político</strong> </h5>
			<p class="card-text">Evalúa cómo se encuentra el país o la localidad en términos políticos, económicos y sociales.</p>
			<a href=" <?php echo base_url(); ?>/assest/imagenes/voto4.png " class="btn btn-primary">ABRIR IMAGEN</a>
		</div>
	</div>
	</div>

	<div class="col-md-4">
		<div class="card" style="width: 18rem;">
		<img src=" <?php echo base_url(); ?>/assest/imagenes/voto5.png" class="card-img-top" alt="...">
		<div class="card-body">
			<h5 class="card-title"> <strong>Acude a las urnas</strong> </h5>
			<p class="card-text">Asegúrate de conocer la fecha, el lugar y los requisitos necesarios para votar.</p>
			<a href=" <?php echo base_url(); ?>/assest/imagenes/voto5.png " class="btn btn-primary">ABRIR IMAGEN</a>
		</div>
	</div>
	</div>

	<div class="col-md-4">
		<div class="card" style="width: 18rem;">
		<img src=" <?php echo base_url(); ?>/assest/imagenes/voto6.png" class="card-img-top" alt="...">
		<div class="card-body">
			<h5 class="card-title"> <strong>Respeta su voto</strong> </h5>
			<p class="card-text">Recuerda que todos tienen derecho a su propia opinión política, respeta las elecciones de los demás</p>
			<a href=" <?php echo base_url(); ?>/assest/imagenes/voto6.png " class="btn btn-primary">VER IMAGEN</a>
		</div>
	</div>
	</div>
</div>
</center>
