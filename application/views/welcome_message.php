<div class="container">
	<div class="d-flex flex-column align-items-center gap-2 px-5">
		<a href="<?php echo site_url('welcome/candidatosPresidenciales') ?>" class="btn btn-outline-primary w-50">
			Reporte Candidatos <span class="fw-bold">Presidenciales</span>
		</a>
		<a href="<?php echo site_url('welcome/candidatosAsambleistasNacionales') ?>"
			class="btn btn-outline-warning w-50">Reporte Asambleístas <span class="fw-bold">Nacionales</span></a>
		<a href="<?php echo site_url('welcome/candidatosAsambleistasProvinciales') ?>"
			class="btn btn-outline-danger w-50">Reporte Asambleístas <span class="fw-bold">Provinciales</span></a>
		<a href="<?php echo site_url('welcome/candidatosGenerales') ?>" class="btn btn-outline-secondary w-50">Reporte
			<span class="fw-bold">
				General de Candidatos</span></a>
	</div>
</div>

<script>
	//read flashdata and alert
	<?php if ($this->session->flashdata('mensaje')): ?>
		alert('<?php echo $this->session->flashdata('mensaje'); ?>');
	<?php endif; ?>
</script>
