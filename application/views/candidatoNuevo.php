<div class="container py-5 my-5 border border-success rounded background-success bg-light-subtle d-grid w-75"
    style="--bs-border-opacity: .1;">
    <form method="POST" action="<?php echo site_url('candidatos/insertarCandidatos') ?>"
        class="container px-5 d-grid w-50">
        <h1 class="text-center display-6 fs-2">Nuevo Candidato</h1>
        <div class="mb-3">
            <label for="dignidad_can" class="form-label">Dignidad</label>
            <select required id="dignidad_can" class="form-control" name="dignidad_can">
                <option value=""> -- Seleccione una opción --</option>
                <option value="Presidencial">Candidato Presidencial</option>
                <option value="AsambleistaNacional">Candidato Asambleísta Nacional</option>
                <option value="AsambleistaProvincial">Candidato Asambleísta Provincial</option>
            </select>
        </div>
        <div class="mb-3">
            <label for="apellidos_can" class="form-label">Apellidos</label>
            <input required type="text" class="form-control" id="apellidos_can" name="apellidos_can">
        </div>
        <div class="mb-3">
            <label for="nombres_can" class="form-label">Nombres</label>
            <input required type="text" class="form-control" id="nombres_can" name="nombres_can">
        </div>
        <div class="mb-3">
            <label for="movimiento_can" class="form-label">Movimiento</label>
            <input required type="text" class="form-control" id="movimiento_can" name="movimiento_can">
        </div>
        <div class="mb-3">
            <div class="row">
                <div class="col-md-6">
                    <label for="latitud_can" class="form-label">Latitud</label>
                    <input required type="text" class="form-control" id="latitud_can" name="latitud_can">
                </div>
                <div class="col-md-6">
                    <label for="longitud_can" class="form-label">Longitud</label>
                    <input required type="text" class="form-control" id="longitud_can" name="longitud_can">
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-success">Enviar</button>
    </form>
</div>
