<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz"
        crossorigin="anonymous"></script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjkNVFrdBOcMli96OZI_1X9zN_4raXvPo&libraries=places&callback=initMap"></script>

    <title>Votaciones</title>
    <link   rel="icon" type="image/png" width="2px" height="2px" href="assest/imagenes/logo.png">
</head>

<style>

  .brand img{
    width: 75px;
    height: auto;
  }

</style>


<body>

    <nav class="navbar navbar-light bg-light" >
    <div class="container-fluid">
      <label for="" class="brand" >
          <a class="navbar-brand" href="<?php echo site_url(); ?>/subcarpetas/inicio"> <img src=" <?php echo base_url(); ?>/assest/imagenes/logo.png " alt="">INFORMATE ECUADOR</a>
      </label>
    </div>
    </nav>

    <header>
        <h1 class="text-center fs-1 fw-bold my-5">Candidatos 2023</h1>
        <hr>
    </header>
