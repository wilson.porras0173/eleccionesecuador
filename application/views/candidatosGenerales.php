<div class="row">
    <div class="col-md-2">
    </div>
    <?php if ($listaCandidatos): ?>
        <div class="col-md-8">
            <h1 class="text-center fs-4 text-primary"><ins>Todos los candidatos a nivel Nacional</ins></h1>
            <div id="map3"
                class="border my-3 rounded text-center display-4 d-flex justify-content-center align-items-center"
                style="height: 65vh;">Refresca la página si no puedes ver el mapa...<div>
                </div>
            <?php else: ?>
                <div class="col-md-8">
                    <h1 class="text-center fs-4 text-primary"><ins>No hay candidatos para asambleistas nacionales</ins></h1>
                </div>
            <?php endif; ?>
            <div class="col-md-2">
            </div>
        </div>

        <script>
            function initMap() {
                var map = new google.maps.Map(document.getElementById('map3'), {
                    zoom: 7,
                    center: {
                        lat: -1.831239,
                        lng: -78.183403
                    }
                });
                <?php if ($listaCandidatos): ?>
                    <?php foreach ($listaCandidatos as $candidato): ?>
                        var marker = new google.maps.Marker({
                            position: {
                                lat: <?php echo $candidato->latitud_can ?>,
                                lng: <?php echo $candidato->longitud_can ?>
                            },
                            map: map,
                            title: '<?php echo $candidato->nombres_can . ' ' . $candidato->apellidos_can ?>'
                        });
                    <?php endforeach; ?>
                <?php else: ?>

                <?php endif; ?>

            }
        </script>
