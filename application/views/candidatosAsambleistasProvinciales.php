<div class="row">
    <div class="col-md-2">
    </div>
    <?php if ($listaCandidatosAsambleistasProvinciales): ?>
        <div class="col-md-8">
            <h1 class="text-center fs-4 text-primary"><ins>Candidatos para Asambleistas Provinciales</ins></h1>
            <div id="map3"
                class="border my-3 rounded text-center display-4 d-flex justify-content-center align-items-center"
                style="height: 65vh;">Refresca la página si no puedes ver el mapa...<div>
                </div>
            <?php else: ?>
                <div class="col-md-8">
                    <h1 class="text-center fs-4 text-primary"><ins>No hay candidatos para asambleistas provinciales</ins>
                    </h1>
                </div>
            <?php endif; ?>
            <div class="col-md-2">
            </div>
        </div>

        <script>
            function initMap() {
                var map = new google.maps.Map(document.getElementById('map3'), {
                    zoom: 7,
                    center: {
                        lat: -1.831239,
                        lng: -78.183403
                    }
                });
                <?php if ($listaCandidatosAsambleistasProvinciales): ?>
                    <?php foreach ($listaCandidatosAsambleistasProvinciales as $candidatoAsambleistaProvincial): ?>
                        var marker = new google.maps.Marker({
                            position: {
                                lat: <?php echo $candidatoAsambleistaProvincial->latitud_can ?>,
                                lng: <?php echo $candidatoAsambleistaProvincial->longitud_can ?>
                            },
                            map: map,
                            title: '<?php echo $candidatoAsambleistaProvincial->nombres_can . ' ' . $candidatoAsambleistaProvincial->apellidos_can ?>'
                        });
                    <?php endforeach; ?>
                <?php else: ?>

                <?php endif; ?>

            }
        </script>
