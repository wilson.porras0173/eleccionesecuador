<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('candidato');
	}

	public function index()
	{
		$this->load->view('templates/header');
		$this->load->view('welcome_message');
		$this->load->view('templates/footer');
	}

	public function candidatoNuevo()
	{
		$this->load->view('templates/header');
		$this->load->view('candidatoNuevo');
		$this->load->view('templates/footer_volver');
	}

	public function candidatosPresidenciales()
	{
		$data['listaCandidatosPresidenciales'] = $this->candidato->selectCandidatosPresidenciales();
		$this->load->view('templates/header');
		$this->load->view('candidatosPresidenciales', $data);
		$this->load->view('templates/footer_volver');
	}

	public function candidatosAsambleistasNacionales()
	{
		$data['listaCandidatosAsambleistasNacionales'] = $this->candidato->selectCandidatosAsambleistasNacionales();
		$this->load->view('templates/header');
		$this->load->view('candidatosAsambleistasNacionales', $data);
		$this->load->view('templates/footer_volver');
	}

	public function candidatosAsambleistasProvinciales()
	{
		$data['listaCandidatosAsambleistasProvinciales'] = $this->candidato->selectCandidatosAsambleistasProvinciales();
		$this->load->view('templates/header');
		$this->load->view('candidatosAsambleistasProvinciales', $data);
		$this->load->view('templates/footer_volver');
	}

	public function candidatosGenerales()
	{
		$data['listaCandidatos'] = $this->candidato->selectCandidatos();
		$this->load->view('templates/header');
		$this->load->view('candidatosGenerales', $data);
		$this->load->view('templates/footer_volver');
	}
}
