<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Candidatos extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('candidato');
    }

    public function insertarCandidatos()
    {
        $data = array(
            'dignidad_can' => $this->input->post('dignidad_can'),
            'apellidos_can' => $this->input->post('apellidos_can'),
            'nombres_can' => $this->input->post('nombres_can'),
            'movimiento_can' => $this->input->post('movimiento_can'),
            'latitud_can' => $this->input->post('latitud_can'),
            'longitud_can' => $this->input->post('longitud_can')
        );
        $this->candidato->insertarCandidato($data);
        //flash data
        $this->session->set_flashdata('mensaje', 'Candidato registrado exitosamente');
        redirect('welcome');
    }


}
