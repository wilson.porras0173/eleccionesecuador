<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Candidato extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function insertarCandidato($data)
    {
        $this->db->insert('candidato', $data);
    }

    public function selectCandidatos()
    {
        $this->db->select('*');
        $this->db->from('candidato');
        $query = $this->db->get();
        return $query->result();
    }

    public function selectCandidatosPresidenciales()
    {
        $this->db->select('*');
        $this->db->from('candidato');
        $this->db->where('dignidad_can', 'Presidencial');
        $query = $this->db->get();
        return $query->result();
    }

    public function selectCandidatosAsambleistasNacionales()
    {
        $this->db->select('*');
        $this->db->from('candidato');
        $this->db->where('dignidad_can', 'AsambleistaNacional');
        $query = $this->db->get();
        return $query->result();
    }

    public function selectCandidatosAsambleistasProvinciales()
    {
        $this->db->select('*');
        $this->db->from('candidato');
        $this->db->where('dignidad_can', 'AsambleistaProvincial');
        $query = $this->db->get();
        return $query->result();
    }
}
